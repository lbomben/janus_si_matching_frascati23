




#Si run number
nrun_Si=$1;

#FERS run number
nrun_FERS=$2;

#number of FERS - default = 2
nFERS=$3;
if [ "$nFERS" == "" ] ; then nFERS=2; fi

#number of channels per FERS - deault = 64
nchFERS=$4;
if [ "$nchFERS" == "" ] ; then nchFERS=64; fi



################################### set directories ###################################

#hbook directory
hbook_dir=/data/insudaq/datadir_Frascati23;
# hbook_dir=Si_raw;

#FERS data directory
dat_dir=/data/insudaq/FERS_Frascati23;
# dat_dir=FERS_raw;

#npz directory (for intermediate files)
npz_dir=/data/insudaq/FERS_Frascati23/npz_files;
# npz_dir=FERS_npz;

#root directory (for intermediate files)
root_dir=/data/insudaq/datadir_Frascati23/root_files;
# root_dir=Si_root;

#final matched files directory
match_dir=/data/insudaq/ascii_Frascati23/FERS_match;
# match_dir=FERS_Si_match;




################################### prepare the root file ###################################

#convert the .hbook files to .root
echo "Converting hbook to root...";
for file in ${hbook_dir}/run${nrun_Si}_*.hbook; do
    echo $file;
    h2root $file >> crap.txt;
done
mv ${hbook_dir}/run${nrun_Si}_*.root ${root_dir};

#hadd in a single file
echo "Appending root files...";
rootfile=${root_dir}/run${nrun_Si}.root
if [ -f ${rootfile} ] ; then rm ${rootfile}; fi
hadd ${rootfile} ${root_dir}/run${nrun_Si}_*.root;

#delete single-spill files
echo "Deleting single-spill files...";
rm ${root_dir}/run${nrun_Si}_*.root;



################################### prepare the raw npz file ###################################

datfile=${dat_dir}/Run${nrun_FERS}_list.dat;
npzfile_raw=${npz_dir}/run${nrun_FERS}_raw.npz;

./read_FERS_raw.py -b ${nFERS} -n ${nchFERS} -o ${npzfile_raw} ${datfile};




################################### prepare the FERS-matched npz file ###################################

npzfile_match=${npz_dir}/run${nrun_FERS}_FERS_match.npz;

./match_FERS.py -b ${nFERS} -o ${npzfile_match} ${npzfile_raw};




################################### prepare the FERS-Si matched files ###################################

matchfile=${match_dir}/run_Si${nrun_Si}_FERS${nrun_FERS}_match.txt;


./match_FERS_Si.py -o ${matchfile} ${npzfile_match} ${rootfile};








