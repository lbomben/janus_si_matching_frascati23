# Janus_Si_matching_Frascati23

To set up:

copy the files in any directory

in match_runs.sh, change the directories if needed (they should be ready for the DAQ PC anyways)


To run:
```
./match_runs.sh <nrun_Si> <nrun_FERS> <nFERS> <nchannels_FERS>
```
nrun_Si = run number of the Si detector DAQ

nrun_FERS = number of the corresponding Janus run

nFERS = number of FERS connected (default=2)

nchannels_FERS = number of channels written per FERS (defauilt=64, depends on the GENERAL channel enable mask set in the AcqMode tab)


# Output data format:

Assuming 2 FERS and 64 channels written

nevent_Si   nevent_FERS0   timestamp_FERS0    64xPHlowgain_FERS0    64xPHhighgain_FERS0    nevent_FERS1   timestamp_FERS1    64xPHlowgain_FERS1    64xPHhighgain_FERS1

