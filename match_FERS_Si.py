#!/usr/bin/env python3


import sys
import os
import numpy as np
from datetime import datetime as dt
import uproot
import argparse
import matplotlib.pyplot as plt




# Arguments parsing
parser = argparse.ArgumentParser()
parser.add_argument("FERS_inputfile", help="FERS input file name")
parser.add_argument("Si_inputfile", help="Si input file name")
#parser.add_argument("-b", "--nboards", help="Number of FERS boards (default = 2)")
#parser.add_argument("-n", "--nchannels", help="Number of channels per FERS (default = 64)")
parser.add_argument("-o", "--output", help="Output file name (default = FERS_Si_match/run_Si<nSi>_FERS_<nFERS>.txt)")

args = parser.parse_args()
FERS_input_file_name = args.FERS_inputfile
Si_input_file_name = args.Si_inputfile
outfile_name = args.output
#nboards = int(args.nboards)
#nchs = int(args.nchannels)


if not args.output:
    outfile_name = "FERS_Si_match/run_Si" + (Si_input_file_name.split("/run")[-1]).split(".")[0] + "_FERS" + (FERS_input_file_name.split("/Run")[-1]).split("_list")[0] + ".txt"





def linearize_Si(ts_Si):
    
    dt=np.diff(ts_Si)
    
    jumps=np.array(np.where(dt<-10000),dtype=int)
    
    if len(jumps)>0:
        for i in jumps:
            j=int(i+1)
            ts_Si[j:]=(ts_Si[j:]+(2**31))
    
    return ts_Si




def group_spill(ts,tid,th):
    
    spill_change= np.array(np.where( np.diff(ts) > th ),dtype=int)+1
    spill_change= np.append(0,spill_change)
    
    #remove eventual doubles
    sc_doubles=np.array(np.where(np.diff(spill_change)==1),dtype=int)+1
    spill_change=np.delete(spill_change,sc_doubles)
    
    tid_spill=[]
    ts_spill=[]
    
    for i in range(1,len(spill_change)):
        
        tid_spill.append( tid[ spill_change[i-1] : spill_change[i] ] )
        ts_spill.append( ts[ spill_change[i-1] : spill_change[i] ] )
        
        
    return [ np.array(tid_spill,dtype=object), np.array(ts_spill,dtype=object) ]


def match_spill(ids,tss,idf,tsf,maxerr,th,t0s,t0f):
    
    #assume in worst case scenario, no more than maxerr trigs in a row are to be discarded
    #pair 1st Si trig with 1st FERS trig, use those a time origins, then find best matches
    
    #deltat between events  is - int this case - 5kish us --> differences significantly lower can be accepted
    
    tss_shift=tss-t0s
    
    tsf_shift=tsf-t0f
    
    idmatch=np.array( [ int(np.argmin( np.abs(ts-tss_shift) ))  for ts in tsf_shift ] )
    idmatch_Si=np.array( [ ids[j] for j in idmatch ] )
    idmatch_FERS=np.copy(idf)
    
    match_err=np.array( [ ( tsf_shift[i] - tss_shift[idmatch[i]] ) for i in range(len(idmatch)) ])
    mismatch=np.where(np.abs(match_err)>th)
    
    idmatch_Si=np.delete(idmatch_Si,mismatch)
    idmatch_FERS=np.delete(idmatch_FERS,mismatch)
    match_err=np.delete(match_err,mismatch)
    
    #ids_match=np.array( [ ids[ int(np.argmin( np.abs(ts-tss_shift) )) ] for ts in tsf_shift ] )
    #MSE= np.sqrt( np.sum( [ ( tsf_shift[i] - tss_shift[idmatch[i]] )**2/len(idmatch) for i in range(len(idmatch)) ] ))
    
    return (np.array((idmatch_FERS,idmatch_Si)),match_err)
            
    #print(idmatch_Si,MSE)
    #print(err,np.mean(err))
            
    
    


if __name__ == "__main__":
    """
    Program to convert a binary file from the Janus software by CAEN
    board A5202 to a ROOT file.
    Required the installation of the uproot library
    """

    if not os.path.isfile(FERS_input_file_name):
        print(f"Error: the file {FERS_input_file_name} does not exist!")
        sys.exit(1)
    if not os.path.isfile(Si_input_file_name):
        print(f"Error: the file {Si_input_file_name} does not exist!")
        sys.exit(1)
        
        
        
    #Open the .npz
    
    FERS_in = np.load(FERS_input_file_name)
    
    data_FERS=FERS_in["data_arr"]
    head_FERS=FERS_in["head"]
    
    
    acqstart_FERS=head_FERS["acq_start"]/1000
    
    #Open the .root
    
    with uproot.open(Si_input_file_name) as Si_in:
        tree=Si_in["h1"]
        Si_ts=(tree["Ivfas_calo"].array(library="np"))[:,27]
        Si_id=tree["Ievent"].array(library="np")
        acqstart_Si=tree["Iatime"].array(library="np")[0]
        
        #print(acqstart_Si)
        #print(tree["Itime"].array(library="np")[0])
        #print(Si_ts[0])
    
    #print(acqstart_FERS)
    #print(data_FERS["trigger_time_stamp"][0,0])
    #t1=np.array(acqstart_Si,dtype=float)+np.array(Si_ts[0:3],dtype=float)/1e6-1.68206e9
    #t2=np.array(acqstart_FERS,dtype=float)+np.array(data_FERS["trigger_time_stamp"][0,0:3],dtype=float)/1e6-1.68206e9
    #print(t1)
    #print(t2)
    #print(t2-t1)
    
    print("Pre-matching:")
    print("Si detectors: ",len(Si_id),"events")
    print("FERS: ",len(data_FERS["trigger_time_stamp"][0]),"events")
    
    #plt.figure()
    #plt.plot(Si_ts)
    #plt.show()
    
    #print(np.shape(Si_ts))
    
    #print(data_FERS["trigger_time_stamp"][0,0])
    #print(Si_ts[0],Si_id[0])
    t0_Si=Si_ts[0]
    t0_FERS=data_FERS["trigger_time_stamp"][0,0]
    #t0_Si=0
    #t0_FERS=(acqstart_Si-acqstart_FERS)*1e60
    
    
    Si_ts=np.array(Si_ts,dtype=np.int64)
    
    #Remove the 32-bit roll-over
    if np.min(np.diff(Si_ts))<-10000:
        Si_ts=linearize_Si(Si_ts)
    
    #plt.figure()
    #plt.plot(Si_ts-Si_ts[0])
    #plt.plot(data_FERS[0]["trigger_time_stamp"]-data_FERS["trigger_time_stamp"][0,0])
    #plt.show()
    
    #timestamps should be in us
    
    
    #plt.figure()
    #plt.plot(np.diff(Si_ts),"*")
    #plt.plot(np.diff(data_FERS[0]["trigger_time_stamp"]))
    ##plt.plot(Si_id[1:],np.diff(Si_ts),"*")
    ##plt.plot(data_FERS["trigger_id"][0,1:],np.diff(data_FERS[0]["trigger_time_stamp"]))
    #plt.show()
    
    #plt.figure()
    #plt.hist(np.diff(Si_ts),bins=100,log=True)
    #plt.show()
    
    #plt.figure()
    #plt.plot(np.diff(data_FERS[0]["trigger_time_stamp"]))
    #plt.show()
    
    #plt.figure()
    #plt.hist(np.diff(data_FERS[0]["trigger_time_stamp"]),bins=100,log=True)
    #plt.show()
    
    #suitable dt threshold for inter-"spill" is around 1e6
    
    [ id_Si_spill, ts_Si_spill ] = group_spill(Si_ts,Si_id,1e6)
    [ id_FERS_spill, ts_FERS_spill ] = group_spill(data_FERS["trigger_time_stamp"][0],data_FERS["trigger_id"][0],1e6)
    
    nev_spill_Si=np.array( [ len(v) for v in id_Si_spill ] )
    nev_spill_FERS=np.array( [ len(v) for v in id_FERS_spill ] )
    
    
    #plt.figure()
    #plt.plot(nev_spill_Si,"*")
    #plt.plot(nev_spill_FERS,"*")
    #plt.show()
    
    #plt.figure()
    #n2watch=int(1)
    #plt.plot(ts_Si_spill[n2watch]-ts_Si_spill[0][0]-(ts_FERS_spill[n2watch]-ts_FERS_spill[0][0]),"*")
    #plt.show()
    
    meanerr=[]
    nass=[]
    
    #outfile_name = "FERS_Si_match/run_Si" + (Si_input_file_name.split("/run")[-1]).split(".")[0] + "_FERS" + (FERS_input_file_name.split("/Run")[-1]).split("_list")[0] + ".txt"
    
    evcnt=0;
    
    with open(outfile_name, 'w') as fout:
        for i in range(len(id_Si_spill)):
        #for i in range(1):
            match,err=match_spill(id_Si_spill[i],ts_Si_spill[i],id_FERS_spill[i],ts_FERS_spill[i],1,3000,t0_Si,t0_FERS)
            #match,err=match_spill(Si_id,Si_ts,data_FERS["trigger_time_stamp"][0],data_FERS["trigger_id"][0],1,3000,t0_Si,t0_FERS)
            if len(err)==0:
                err=[0]
            meanerr.append(np.mean(err))
            nass.append(len(match[0]))
            
            #print(np.shape(match))
            
            if i%10==0:
                print(f"matching spill {i}...")
                
            for j in range(len(match[0])):
                
                idf=match[0,j]
                ids=match[1,j]
                
                
                evt=data_FERS[:, data_FERS["trigger_id"][0] == idf ]
                
                line=f"{int(ids)} "
                
                #print("match for FERS event",idf)
                
                
                for d in evt:
                    
                    tid=d["trigger_id"][0]
                    tts=d["trigger_time_stamp"][0]
                    phlg=((d["ch_data"]["lg_pha"])[0])
                    phhg=(d["ch_data"]["hg_pha"])[0]
                    
                    line = line + f"{tid} {tts} " + "".join(str(phlg)).replace("[","").replace("]","").replace("\n","") + " " + "".join(str(phhg)).replace("[","").replace("]","").replace("\n","") + " "
                line=line+"\n"
                #print(line)
                fout.write(line)
                
    
    print("Post-matching:")
    print(np.sum(nass),"events")
    #plt.figure()
    #plt.plot(meanerr)
    #plt.show()
    
    #plt.figure()
    #plt.plot(nass)
    #plt.show()
    
    
    #print(match)s
    #print(np.shape(match))
    #match_spill(id_Si_spill[1],ts_Si_spill[1],id_FERS_spill[1],ts_FERS_spill[1],1,10,ts_Si_spill[0][0],ts_FERS_spill[0][0])
    #match_spill(id_Si_spill[2],ts_Si_spill[2],id_FERS_spill[2],ts_FERS_spill[2],1,10,ts_Si_spill[0][0],ts_FERS_spill[0][0])
    #match_spill(id_Si_spill[3],ts_Si_spill[3],id_FERS_spill[3],ts_FERS_spill[3],1,10,ts_Si_spill[0][0],ts_FERS_spill[0][0])
    #match_spill(id_Si_spill[20],ts_Si_spill[20],id_FERS_spill[20],ts_FERS_spill[20],1,10,ts_Si_spill[0][0],ts_FERS_spill[0][0])
    print("data saved in",outfile_name)
    
    




