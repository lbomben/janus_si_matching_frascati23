#!/usr/bin/env python3


import sys
import os
import numpy as np
from datetime import datetime as dt
import uproot
import argparse
import matplotlib.pyplot as plt


# Arguments parsing
parser = argparse.ArgumentParser()

parser.add_argument("inputfile", help="Binary input file name")
parser.add_argument("-b", "--nboards", help="Number of FERS boards (default = 2)")
parser.add_argument("-n", "--nchannels", help="Number of channels per FERS (default = 64)")
parser.add_argument("-o", "--output", help="Output file name (default = FERS_npz/<sameasinput>_raw.npz)")

args = parser.parse_args()
input_file_name = args.inputfile
outfile_name = args.output




if not args.nboards:
    nboards=int(2)
else:
    nboards = int(args.nboards)
    
if not args.nchannels:
    nchs=int(64)
else:
    nchs = int(args.nchannels)
    
if not args.output:
    outfile_name = "FERS_npz/" + input_file_name.split("/")[-1][:-4] + "_raw.npz"


# File header according to the Janus software manual pag 38
#
# Binary file structure memo:
# 
# [FILE HEADER] +
#   nevents x 
#            [EVENT HEADER] + 
#            64 x [DATA]


# Common to all acquisition modes:

Janus_file_header = np.dtype(
    [
        ("data_format_version", ("B", 2)),  # 16 bits, Data Format Version
        ("software_version", ("B", 3)),  # 24 bits, Data Format Version
        ("acq_mode", np.dtype("B")),  # 8  bits, Acq Mode
        ("time_unit", np.dtype("B")),  # 8  bits, Time Unit # new v3
        ("energy_n_channels",np.dtype(">u2")),  # 16  bits, Energy NChannels # new v3
        ("time_conversion", np.dtype("<f4")), # 32 bits, Time Conversion # new v3
        ("acq_start", np.dtype("u8"))  # 64 bits, Start Acquisition
    ]
)

# Acquisition modes dict (just to print them)

acq_modes = {
    1: "Spectroscopy mode",
    2: "Timing Mode",
    3: "Spectroscopy + Timing Mode",
    4: "Counting Mode"
}

# Dictionaries with the 4 different event schema
# TODO: it would be convenient to put it in an
# external file (json)

# before, the event schema to be repeated 64 times

Janus_event_data_schema = {
    1: np.dtype(  # data for event in spectroscopy mode
        [
            ("channel_id", np.dtype("B")),
            ("data_type", np.dtype("B")),
            ("lg_pha", np.int16),
            ("hg_pha", np.int16),
        ]
    )
}


# now the general event schema






Janus_event_schema = {
    1: np.dtype(
        [
            ("event_size", np.dtype("u2")),
            ("board_id", np.dtype("B")),
            ("trigger_time_stamp", np.dtype("f8")),
            ("trigger_id", np.dtype("u8")),
            ("channel_mask", np.dtype("u8")),
            ("ch_data", (Janus_event_data_schema[1], nchs))
        ]
    )
}



# Some useful formatting functions

def better_print(byte_seq):
    '''Just a nice representation of
    arrays of 1 byte integers (file header)
    Example "1.1" instead of "array[1,1]"'''

    return ".".join([str(n) for n in byte_seq])


def better_timestamp_print(millisec_timestamp):
    return dt.fromtimestamp(millisec_timestamp / 1000).strftime("%c")


def print_file_info(file_header_data):
    print("Data Format Version: " + better_print(file_header_data["data_format_version"][0]))
    print("Software Version: " + better_print(file_header_data["software_version"][0]))
    print("Acquisition Mode: " + str(file_header_data["acq_mode"][0]))
    print("Time Unit: " + str(file_header_data["time_unit"][0]))
    print("Energy N Channels: " + str(file_header_data["energy_n_channels"][0]) + " bins")
    print("Time Conversion: " + str(file_header_data["time_conversion"][0]) + " ns/LSB")
    #print("Acquisition Time: " + better_timestamp_print(file_header_data["acq_start"]))








if __name__ == "__main__":
    """
    Program to convert a binary file from the Janus software by CAEN
    board A5202 to a ROOT file.
    Required the installation of the uproot library
    """

    if not os.path.isfile(input_file_name):
        print(f"Error: the file {input_file_name} does not exist!")
        sys.exit(1)

    # Open the file and read the data
    
    with open(input_file_name, "rb") as fp:

        # File header, 1 time
        head = np.fromfile(fp, dtype=Janus_file_header, count=1)
        
        print_file_info(head)

        acq_mode = head["acq_mode"][0]
        
        data_mix = np.fromfile(fp, dtype=Janus_event_schema[acq_mode], count=-1)
        
    
    data=[ data_mix[data_mix["board_id"]==nb] for nb in range(nboards) ]
    
    
    trigid=[ v["trigger_id"] for v in data ]
    tstamp=[ v["trigger_time_stamp"] for v in data ]
    nev=[ len(v) for v in trigid ]
    lmin=np.min(np.array(nev))
    for i in range(nboards):
        print(nev[i]," events in board ",i)
    
    
    #some diagnostic plots
    #plt.figure(1)
    #for i in range(nboards):
        #plt.plot(tstamp[i])
        #plt.show()
        
    #plt.figure(2)
    #for i in range(nboards):
        #plt.plot(tstamp[i][lmin-10:-1])
        #plt.show()
        
    #plt.figure(3)
    #for i in range(nboards):
        #plt.plot(np.diff(tstamp[i]))
        #plt.show()
        
    
    #plt.figure(4)
    #for i in range(1,nboards):
        #plt.plot(tstamp[i][0:lmin]-tstamp[0][0:lmin])
        #plt.show()
        
    
    np.savez(outfile_name,*data,head=head)
    
    print("data saved in",outfile_name)
        
        
        
    
    



































    ## Create the output ROOT file (ask before overwrite)
    ## default output file name 
    ## (drop absolute path and save into the same dir)
    #if not args.output:
        #outfile_name = input_file_name.split("/")[-1][:-4] + ".root"
    #if os.path.isfile(outfile_name):
        #answer = input(f"Output file {outfile_name} already exists: overwrite? [Y/n] ")
        #if answer in ["", "Y", "y", "yes", "Yes"]:
            #pass
        #else:
            #print("Exit.")
            #sys.exit(1)


 
    #with uproot.recreate(outfile_name) as f:

        ## ROOT TTree object

        #f["t"] = {
            #"trigID" : data['trigger_id'],
            #"triggerTimeStamp" : data['trigger_time_stamp'],
            #"boardID": data['board_id'],
            #"channelMask": data['channel_mask'],
##            "nev5bits": nev5bits,
            #"lg": data["ch_data"]["lg_pha"],
            #"hg": data["ch_data"]["hg_pha"]
        #}

        ## Some strings (TObjStrings) attached to the TTree

        #f["data_format_version"] = better_print(head["data_format_version"][0])
        #f["software_version"] = better_print(head["software_version"][0])
        #f["acq_mode"] = acq_modes[head["acq_mode"][0]]
        ##f["acq_time"] = better_timestamp_print(head["acq_start"])
        #f["acq_time_unix_ms"] = str(head["acq_start"][0])
        ##f["time_unit"] = better_timestamp_print(head["time_unit"])
        #f["energy_n_channels"] = str(head["energy_n_channels"][0])
        #f["time_conversion"] = str(head["time_conversion"][0])

        #print(f"\n => File {outfile_name} written.")
