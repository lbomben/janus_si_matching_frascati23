#!/usr/bin/env python3


import sys
import os
import numpy as np
from datetime import datetime as dt
import uproot
import argparse
import matplotlib.pyplot as plt




# Arguments parsing
parser = argparse.ArgumentParser()
parser.add_argument("inputfile", help="Binary input file name")
parser.add_argument("-b", "--nboards", help="Number of FERS boards (default = 2)")
#parser.add_argument("-n", "--nchannels", help="Number of channels per FERS (default = 64)")
parser.add_argument("-o", "--output", help="Output file name (default = FERS_npz/<sameasinput>_FERS_match.npz)")

args = parser.parse_args()
input_file_name = args.inputfile
outfile_name = args.output
#nchs = int(args.nchannels)


if not args.nboards:
    nboards=int(2)
else:
    nboards = int(args.nboards)

if not args.output:
    outfile_name = "FERS_npz/" + input_file_name.split("/")[-1][:-4] + "_FERS_match.npz"
    

def remove_doubles(data,th):
    
    for i in range(nboards):
        dt=np.diff(data[i]["trigger_time_stamp"])
        doubles_loc=np.where(dt<th)
        doubles_loc=np.array(doubles_loc,dtype=int)
        #data[i]=np.delete(data[i],np.concatenate( [doubles_loc,doubles_loc+1] ))
        data[i]=np.delete(data[i],doubles_loc+1)
        
    return data
        


def match_FERS(data,th):
    
    
    lmin=np.min(np.array([ len(d) for d in data]))
    
    ts=[ d["trigger_time_stamp"] for d in data ]
    
    dt=[ ts[0][0:lmin] - ts[i][0:lmin] for i in range(1,nboards) ]
    
    oos=[ np.abs(d)>th for d in dt ]
    n_oos=np.array([np.sum(o) for o in oos])
    
    while np.sum(n_oos)>0:
        
        mybrd=np.argmax(n_oos>0)
        myev=np.argmax(oos[mybrd])
        
        mydt=dt[mybrd][myev]
        
        if(mydt>0):
            data[0]=np.delete(data[0],myev)
        
        if(mydt<0):
            data[mybrd+1]=np.delete(data[mybrd+1],myev)
        
        lmin=np.min(np.array([ len(d) for d in data]))
        
        ts=[ d["trigger_time_stamp"] for d in data ]
        
        dt=[ ts[0][0:lmin] - ts[i][0:lmin] for i in range(1,nboards) ]
        
        oos=[ np.abs(d)>th for d in dt ]
        n_oos=np.array([np.sum(o) for o in oos])
    
    
    return [ d[0:lmin] for d in data ]





if __name__ == "__main__":
    """
    Program to convert a binary file from the Janus software by CAEN
    board A5202 to a ROOT file.
    Required the installation of the uproot library
    """

    if not os.path.isfile(input_file_name):
        print(f"Error: the file {input_file_name} does not exist!")
        sys.exit(1)
        
        
        
    #Oen the npz
    
    filein = np.load(input_file_name)
    
    data=[ filein[f"arr_{i}"] for i in range(len(filein.files)-1) ]


    head=filein["head"]

    
    nev=[ len(v) for v in data ]
    for i in range(nboards):
        print(nev[i]," events in board ",i)
    
    data=remove_doubles(data,4000)
    
    print("After doubles removal:")
    nev=[ len(v) for v in data ]
    for i in range(nboards):
        print(nev[i]," events in board ",i)
        
        
    
    if nboards>1:
        data=match_FERS(data,0.01)
    
    print("After inter-FERS matching:")
    nev=[ len(v) for v in data ]
    for i in range(nboards):
        print(nev[i]," events in board ",i)
        
    data_arr=np.vstack(data)
    #print(np.shape(data_arr))
    
    
    np.savez(outfile_name,head=head,data_arr=data_arr)
    
    print("data saved in",outfile_name)




